/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

// #snip "doc"
// Matchers are essentially enhanced Maps: they bind keys to values. The
// difference is that the key-matching operation for Matchers can be anything,
// whereas for Maps it's always `===`. Still, Maps are so useful that we need
// a matcher that replicates their behavior. `Id` is that matcher. It's a simple
// wrapper around a `Map`.

class Id {
  constructor () {
    this._map = new Map()
  }

  // Maps have a `get(key)` method that returns the value bound to `key`.
  // Matchers need two such methods: one for pattern matching, and one for
  // getting the value for a pattern (literally). These are the `match` and
  // `pattern` methods, respectively.

  // `match` is a generator that yields results in order from best match to
  // worst. In our case here we will either yield one value or none: either
  // there's a value at the given key, or there isn't. But more generally, match
  // methods can yield 0 or more results.
  * match (obj) {
    const v = this._map.get(obj)
    if (v !== undefined) {
      yield v
    }
  }

  // `pattern` is a getter that's used to bind a value to a pattern. Matchers
  // don't store these values directly; they store "container" objects on which
  // the caller can attach values. This layer of indirection is what allows
  // NMatch to chain matchers. `pattern` returns the container object associated
  // to the given pattern, first creating it if it doesn't exist.
  pattern (obj) {
    let v = this._map.get(obj)
    if (v === undefined) {
      v = {}
      this._map.set(obj, v)
    }

    return v
  }
}
// #snip

module.exports = Id
