/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const Id = require('./id')

// #snip "doc"
// All `Any` does is one type check and one for loop; the real work is delegated
// to the matcher it wraps.
class Any {
  constructor (matcher = new Id()) {
    this._matcher = matcher
  }

  pattern (pattern) {
    return this._matcher.pattern(pattern)
  }

  * match (values) {
    if (values == null || typeof values[Symbol.iterator] !== 'function' || typeof values === 'string') {
      throw new Error('Argument must be a non-string iterable')
    }
    for (let value of values) {
      yield * this._matcher.match(value)
    }
  }
}
// #snip

module.exports = Any
