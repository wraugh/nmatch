/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

// #snip "doc"
// Consider the difference between
//
//     find / -path /usr/bin/node
//
// and
//
//     ls /usr/bin/node
//
// The first visits every file on the system and checks if their name is
// "/usr/bin/node". It takes 15s to run on my laptop.
//
// The second takes 3ms to run. It doesn't visit every file. It visits `/usr`,
// `/usr/bin`, and `/usr/bin/node`. It can do this because directories are
// organized in a tree structure, and file names represent paths through that
// tree. It just needs to walk the path one `/`-delimited node at a time.
//
// Inspired by this, we can build a Paths matcher that stores its patterns in a
// tree, and does its matching by traversing that tree.
//
// We'll support three kinds of path elements: literals, wildcards, and
// super-wildcards. Literals are strings that match only the same string,
// wildcards match any string, and super-wildcards match any string, even
// across path separators. Take for instance this four pattern set:
//
// | Pattern    | "/foo/bar" | "/foo/baz" | "/foo/bar/baz" |
// | ---------- | ---------- | ---------- | -------------- |
// | `/foo/bar` | Match      | No match   | No match       |
// | `/foo/baz` | No match   | Match      | No match       |
// | `/foo/*`   | Match      | Match      | No match       |
// | `/foo/**`  | Match      | Match      | Match          |
//
// We can represent these four patterns as a tree with six nodes:
//
//     /
//     └── foo/
//         ├── bar
//         ├── baz
//         ├── *
//         └── **
//
// Before we get around to writing our matcher, let's **implement that Node
// object**. It will handle tree traversal and matching individual path
// segments.
//
// We'll need a way for a Node to reference a child Node:
const nextKey = Symbol('nmatch/path nextNode')

// And we'll need a way to tell whether a Node is the end of a path.
// This isn't as simple as checking whether it has any children. In the
// pattern set `("/foo", "/foo/bar")`, the Node "/foo" has a child Node,
// but it's also the end of a path. We need to attach a flag on every
// node that's a valid endpoint:
const endKey = Symbol('nmatch/path isEnd')

// A Node needs to keep a list of its children. We can simply use an Object for
// this purpose: the key is the child's path part string (e.g. "foo" or
// "bar"), and the value is the child Node. We could treat `*` and `**` as
// special keys for wildcards and super-wildcards, but because we let users
// define custom wildcard tokens (via test functions), it's possible for `*` or
// `**` to be legitimate literal path segments. So we'll use separate variables
// to hold those two.
class Node {
  constructor (wildcardTest, superWildcardTest) {
    this._isWildcard = wildcardTest
    this._isSuperWildcard = superWildcardTest
    this._literals = {}
    this._wildcard = undefined
    this._superWildcard = undefined
  }

  // Matching is simple: we want to find an exact match, a wildcard match,
  // and/or a super-wildcard match, in that order. So we'll ask three questions:
  // "is there an exact match? a wildcard? a super-wildcard?". For any "yes", we
  // yield the matching child Node. If we're at the end of the path, this means
  // yielding the child directly; otherwise it means recursively matching the
  // rest of the path against the child Node.
  * match (pathParts) {
    for (let m of [this._literals[pathParts[0]], this._wildcard, this._superWildcard]) {
      if (m === undefined) continue

      const isLeaf = (pathParts.length === 1 || m === this._superWildcard) &&
        m[endKey] !== undefined
      if (isLeaf) yield m

      const isBranch = pathParts.length > 1 && m[nextKey] !== undefined
      if (isBranch) yield * m[nextKey].match(pathParts.slice(1))
    }
  }

  // When adding paths to our Paths matcher via its `pattern` method, we'll
  // need an analogous `part` method on each Node: a function that returns a
  // handle to the part's container, and creates it as needed. There are two
  // modes: if the part is an endpoint, we just set the endpoint flag on the
  // container. Otherwise we put a new child Node in the container.
  part (part, isEndpoint = false) {
    let [p, partType] = this._getOrCreate(part)

    if (isEndpoint) {
      p[endKey] = true
    } else {
      if (partType === 'super-wildcard') {
        throw new Error('Super wildcards must be the last path element')
      }
      if (p[nextKey] === undefined) {
        p[nextKey] = new Node(this._isWildcard, this._isSuperWildcard)
      }
    }

    return p
  }

  // Our part containers are just fresh Objects, i.e. `{}`.
  _getOrCreate (part) {
    let partType = this._parseType(part)
    let p = this._getExisting(part, partType)
    if (p === undefined) {
      if (partType === 'super-wildcard') {
        this._superWildcard = {}
        p = this._superWildcard
      } else if (partType === 'wildcard') {
        this._wildcard = {}
        p = this._wildcard
      } else {
        this._literals[part] = {}
        p = this._literals[part]
      }
    }

    return [p, partType]
  }

  _getExisting (part, type) {
    switch (type) {
      case 'super-wildcard':
        return this._superWildcard
      case 'wildcard':
        return this._wildcard
      case 'literal':
        return this._literals[part]
    }
  }

  _parseType (part) {
    if (this._isSuperWildcard(part)) {
      return 'super-wildcard'
    }
    if (this._isWildcard(part)) {
      return 'wildcard'
    }

    return 'literal'
  }
}

// The Node class we've just defined does all the heavy lifting when matching:
// it's the one that walks the tree and finds matches. The Paths class' main
// job is to split paths into parts, but that's done with a user-defined function.
// So there's not much left for it to do.
class Paths {
  constructor (separator = '/', wildcard = '*', superWildcard = '**') {
    this._separate = typeof separator === 'function'
      ? separator : this._getDefaultSeparator(separator)

    const isWildcard = typeof wildcard === 'function'
      ? wildcard : this._getDefaultPartTest(wildcard)

    const isSuperwildcard = typeof superWildcard === 'function'
      ? superWildcard : this._getDefaultPartTest(superWildcard)

    this._pathStart = new Node(isWildcard, isSuperwildcard)
  }

  // When adding a new pattern, we walk along the tree and create missing Nodes
  // as needed with the `Node.part` method.
  pattern (pattern) {
    const pathParts = this._separate(pattern)
    var step = this._pathStart

    while (pathParts.length > 1) {
      step = step.part(pathParts.shift())[nextKey]
    }

    return step.part(pathParts.shift(), true)
  }

  * match (path) {
    yield * this._pathStart.match(this._separate(path))
  }

  _getDefaultSeparator (pattern) {
    if (!pattern) {
      return path => [path]
    }

    return path => path.split(pattern)
  }

  _getDefaultPartTest (pattern) {
    if (!pattern) {
      return part => false
    }

    return pattern instanceof RegExp
      ? part => pattern.test(part)
      : part => part === pattern
  }
}
// #snip

module.exports = Paths
