#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const { code2doc } = require('marcco')
const { rewriteRequires } = require('public-requires')
const { Snippin } = require('snippin')

const rr = src => rewriteRequires('nmatch', __dirname, `${__dirname}/test`, src)
const doc = src => code2doc(rr(src), { codePrefix: '~~~javascript' })
const deets = (summary, content) =>
  `<details><summary>${summary}</summary>\n\n${content}\n\n</details>`

const s = new Snippin()
const pin = (file, snippet) => (s.getSnippetsSync(file))[snippet]

process.chdir(__dirname)
fs.writeFileSync('./README.md', `${pin('test/nmatch.js', 'intro')}

${doc(pin('test/nmatch.js', 'intro II'))}

${deets('More details', doc(pin('test/nmatch.js', 'details')))}

Built-in Matchers
-----------------

NMatch is powered by _matchers_, objects whose job it is to store patterns
and perform matching against them. Let's look at the built-in matchers before
discussing how you can build your own.

### Id

${doc(pin('test/id.js', 'intro'))}

${deets('How it\'s implemented', doc(pin('matchers/id.js', 'doc')))}

### Paths

${doc(pin('test/paths.js', 'intro'))}

${deets('How it\'s implemented', doc(pin('matchers/paths.js', 'doc')))}

### Any

${doc(pin('test/any.js', 'intro'))}

${deets('How it\'s implemented', doc(pin('matchers/any.js', 'doc')))}

Creating your own Matcher
-------------------------

${doc(pin('test/nmatch.js', 'matcher intro'))}

${deets('How matchers are used: NMatch\'s implementation', doc(pin('index.js', 'doc')))}

Contributing
------------

This project is deliberately left imperfect to encourage you to participate in
its development. If you make a Pull Request that

 - explains and solves a problem,
 - follows [standard style](https://standardjs.com/), and
 - maintains 100% test coverage

it _will_ be merged: this project follows the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

To make sure your commits follow the style guide and pass all tests, you can add

    ./.pre-commit

to your git pre-commit hook.
`)
