/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

// #snip "doc"
// NMatch stores patterns in a tree-like structure, with the leftmost matcher as
// the root of the tree, and each subsequent matcher one level down in the tree.
// If you're working with a lot of patterns, you'll typically want to put your
// most discriminating matchers first to speed up matching operations.

const valueKey = Symbol('nmatch-value')

class NMatch {
  constructor (matcherMakers) {
    this._checkMatcherMakers(matcherMakers)
    this._matcherMakers = matcherMakers
    this._root = this._matcherMakers[0]()
  }

  // To set a value, we walk down the pattern tree, creating missing nodes as
  // needed, and set the value on the end node.
  set (...patterns) {
    const value = patterns.pop()
    this._checkParts(patterns)
    let cur = this._root
    const nexts = this._matcherMakers.slice(1)
    let pattern, next
    while ((pattern = patterns.shift(), next = nexts.shift())) {
      cur = cur.pattern(pattern)
      if (cur[valueKey] === undefined) {
        cur[valueKey] = next()
      }
      cur = cur[valueKey]
    }
    cur.pattern(pattern)[valueKey] = value
  }

  // To match an instance, we also walk down the pattern tree, but each step
  // forward is a matching operation that returns 0 or more child nodes. If the
  // first child node doesn't match, we try the second, and so on until we know
  // for sure whether we have a match or not.
  match (...instances) {
    this._checkParts(instances)

    return this._rMatch(this._root, instances)
  }

  _rMatch (matcher, instances) {
    const rest = instances.slice(1)

    for (let candidate of matcher.match(instances[0])) {
      if (rest.length === 0) {
        return candidate[valueKey]
      }

      let subMatch = this._rMatch(candidate[valueKey], rest)
      if (subMatch !== null) {
        return subMatch
      }
    }

    return null
  }

  _checkMatcherMakers (matcherMakers) {
    if (matcherMakers.length < 1) {
      throw new Error('At least one matcher is required')
    }
    for (let matcher of matcherMakers) {
      if (typeof matcher !== 'function') {
        throw new Error('MatcherMakers must be functions')
      }
      let m = matcher()
      if (typeof m.pattern !== 'function' || typeof m.match !== 'function') {
        throw new Error('MatcherMakers must have a `pattern` and a `match` function')
      }
    }
  }

  _checkParts (parts) {
    if (parts.length !== this._matcherMakers.length) {
      throw new Error('Bad part count. ' +
                `Expected ${this._matcherMakers.length}, have ${parts.length}`)
    }
  }
}
// #snip

module.exports = NMatch
