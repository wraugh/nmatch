/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const tap = require('tap')

// #snip "intro"
// `Paths` matches paths against patterns that can contain wildcards. It makes
// use of the hierarchical nature of paths to store and match patterns
// efficiently. If you run a benchmark test please share your results.

const NMatch = require('../')
const Paths = require('../matchers/paths')

const p = new NMatch([ () => new Paths() ])

// Patterns are made up of literals, wildcards, and super-wildcards.
// Literals match when strings are exactly equal, obv:
p.set('lit', 1)
p.set('lit/foo', 2)
p.set('lit/bar', 3)

tap.equals(p.match('lit/bar'), 3)
tap.equals(p.match('lit/quux'), null)

// Wildcards (`*`) will match anything in a single path segment
p.set('wild/*', 4)
p.set('wild/*/foo', 4.1)

tap.equals(p.match('wild/foo'), 4)
tap.equals(p.match('wild/bar/foo'), 4.1)
tap.equals(p.match('wild/foo/bar'), null)

// Super-wildcards (`**`) will match anything including further path segments
p.set('super/**', 5)

tap.equals(p.match('super/foo'), 5)
tap.equals(p.match('super/foo/bar'), 5)

// It's an error if you try to add more patterns after a super-wildcard
tap.throws(() => p.set('super/**/useless suffix', 5))

// Wildcards and super-wildcards apply to path segments as a whole; there are
// no partial string matches. The following example patterns are just literals:
p.set('lit/w*', 6)
p.set('lit/w**', 7)

tap.equals(p.match('lit/wat'), null)
tap.equals(p.match('lit/w*'), 6)
tap.equals(p.match('lit/w**'), 7)

// Only patterns can have wildcards. When you call `match`, the argument is just
// a literal.
p.set('foo/bar', 8)
p.set('foo/baz', 9)

tap.equals(p.match('foo/*'), null)

// When more than one pattern matches, they are sorted in order from tightest
// match to loosest match. A literal matches more tightly than a wildcard,
// which in turn matches more tightly than a super-wildcard. NMatch will
// return the first hit.
p.set('foo/bar', 'foobar')
p.set('foo/*', 'splat')
p.set('foo/*/**', 'splat super')
p.set('foo/**', 'super')

tap.equals(p.match('foo/bar'), 'foobar')
tap.equals(p.match('foo/foo'), 'splat')
tap.equals(p.match('foo/bar/baz'), 'splat super')

// #### Customizing
//
// **The default path separator is `/`**, but you can change that
const ns = new NMatch([ () => new Paths('::') ])
ns.set('Foo::*', 1)
tap.equals(ns.match('Foo::Bar'), 1)

// You can use a regex to specify the separator
const words = new NMatch([ () => new Paths(/\s+/) ])
words.set('Ho * Ho!', 1)
tap.equals(words.match('Ho   Ho   Ho!'), 1)

// Or you can define your own separator function
const urls = new NMatch([ () => new Paths(path => {
  const parts = path.replace(/\?.*/, '').split('/').slice(1)
  return parts.length > 0 ? parts : ['']
})])
urls.set('/foo/bar', 1)
tap.equals(urls.match('/foo/bar?baz=quux'), 1)

// You can use a falsy separator to turn off path separation altogether
const lit = new NMatch([ () => new Paths('') ])
lit.set('foo/*', 1)
tap.equals(lit.match('foo/bar'), null)
tap.equals(lit.match('foo/*'), 1)

// **The default wildcard symbol is `*`**, but you can change that
const madLibs = new NMatch([ () => new Paths(' ', '____') ])
madLibs.set('The ____ brown ____', 1)
tap.equals(madLibs.match('The quick brown fox'), 1)

// You can use a regex to specify what counts as a wildcard
const ruby = new NMatch([ () => new Paths('/', /^:\w+/) ])
ruby.set('foos/:id', 1)
tap.equals(ruby.match('foos/123'), 1)

// Or you can pass in your own function to determine if a string is a wildcard
const params = ['size', 'color']
const fw = new NMatch([ () => new Paths(':', str => params.includes(str)) ])
fw.set('towels:size:color', 1)
tap.equals(fw.match('towels:large:green'), 1)

// You can use a falsy value to turn off wildcards altogether
const quiteLit = new NMatch([ () => new Paths('.', null) ])
quiteLit.set('*.*', 1)
tap.equals(quiteLit.match('cmd.exe'), null)
tap.equals(quiteLit.match('*.*'), 1)

// **The default super-wildcard symbol is** `**`, but you can change that
const trunc = new NMatch([ () => new Paths(' ', '____', '...') ])
trunc.set('The ____ brown ...', 1)
tap.equals(trunc.match('The quick brown fox jumped'), 1)

// You can use a regex to specificy what counts as a super-wildcard
const apiGateway = new NMatch([ () => new Paths('/', /^\{\w+\}$/, /^\{\w+\+\}$/) ])
apiGateway.set('{proxy+}', 1)
tap.equals(apiGateway.match('foo/bar'), 1)

// Or you can pass in your own function to determine if a string is a super-wildcard
const anything = new NMatch([ () => new Paths(0, 0, str => true) ])
anything.set('really anything', 1)
tap.equals(anything.match('welp'), 1)

// You can use a falsy value to turn off super-wildcards altogether
const http = new NMatch([ () => new Paths('/', '*', false) ])
http.set('*', 1)
http.set('**', 2)
tap.equals(http.match('wild'), 1)
tap.equals(http.match('not/wild'), null)
tap.equals(http.match('**'), 2)
// #snip

// And now the gory details:
let pm = new Paths()
pm.pattern('foo').id = 'foo'
tap.same([...pm.match('bar')], [])
tap.same([...pm.match('foo')], [{ id: 'foo' }])

pm.pattern('foo/bar').id = 'foobar'
tap.same([...pm.match('foo/bar')], [{ id: 'foobar' }])

pm.pattern('').id = 'empty'
tap.same([...pm.match('')], [{ id: 'empty' }])

pm.pattern('bar/baz/quux').id = 'lit lit lit'
pm.pattern('bar/baz/*').id = 'lit lit splat'
pm.pattern('bar/baz/**').id = 'lit lit super'
pm.pattern('bar/*/quux').id = 'lit splat lit'
pm.pattern('bar/*/*').id = 'lit splat splat'
pm.pattern('bar/*/**').id = 'lit splat super'
pm.pattern('*/baz/quux').id = 'splat lit lit'
pm.pattern('*/baz/*').id = 'splat lit splat'
pm.pattern('*/baz/**').id = 'splat lit super'
pm.pattern('*/*/quux').id = 'splat splat lit'
pm.pattern('*/*/*').id = 'splat splat splat'
pm.pattern('*/*/**').id = 'splat splat super'
pm.pattern('bar/**').id = 'lit super'
pm.pattern('*/**').id = 'splat super'
pm.pattern('**').id = 'super'
tap.same([...pm.match('bar/baz/quux')], [
  { id: 'lit lit lit' },
  { id: 'lit lit splat' },
  { id: 'lit lit super' },
  { id: 'lit splat lit' },
  { id: 'lit splat splat' },
  { id: 'lit splat super' },
  { id: 'lit super' },
  { id: 'splat lit lit' },
  { id: 'splat lit splat' },
  { id: 'splat lit super' },
  { id: 'splat splat lit' },
  { id: 'splat splat splat' },
  { id: 'splat splat super' },
  { id: 'splat super' },
  { id: 'super' }
])
