/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const tap = require('tap')

// #snip "intro"
// The `Any` matcher is a wrapper that lets you match more than one instance at
// a time. You pass its constructor another matcher (e.g. `Id` or `Paths`), and
// you pass its `match` method an array of instances. It will loop through that
// array and call `match` on the underlying matcher, returning the first match
// it finds. This is useful when you need to support fallbacks or aliases.
const NMatch = require('../')
const Any = require('../matchers/any')
const Paths = require('../matchers/paths')

const any = new NMatch([ () => new Any(new Paths()) ])
any.set('/foo/*', 'foo')
any.set('/bar/*', 'bar')

tap.is(any.match(['/foo/123', '/bar/456']), 'foo')
tap.is(any.match(['/bar/456', '/foo/123']), 'bar',
  "Matches are searched for in the order they're given")

tap.is(any.match(['/baz/789', '/quux/012']), null)
tap.is(any.match(['/baz/789', '/quux/012', '/foo/345']), 'foo')

// `Any.match` takes an iterable for argument, and will throw an Error
// otherwise. It doesn't count strings as iterables.
tap.is(any.match([]), null,
  'Attempting to match nothing at all is the same as finding no match')
tap.throws(() => any.match(123),
  'Argument must be a non-string iterable')
tap.throws(() => any.match('/foo/123'),
  'Argument must be a non-string iterable')

// If you don't specify a matcher, `Any` will default to using `Id`.
const defaultAny = new NMatch([ () => new Any() ])
defaultAny.set(any, any)
defaultAny.set(tap, tap)
tap.is(defaultAny.match([any, 123, tap]), any)
tap.is(defaultAny.match([123, tap, any]), tap)
// #snip
