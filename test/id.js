/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'

const tap = require('tap')
const NMatch = require('../')

// #snip "intro"
// The most basic matcher is `Id`, whose "patterns" can be any object, and
// whose "matching" is just applying the strict equality (`===`) operator. It's
// the equivalent of a `Map`, and in fact uses a `Map` internally to store
// patterns and values.

const Id = require('../matchers/id')

const id = new NMatch([ () => new Id() ])

id.set(123, 123)
id.set('Abc', 'Abc')

tap.is(id.match(123), 123)
tap.is(id.match(456), null)
tap.is(id.match('Abc'), 'Abc')
tap.is(id.match('A'), null)

const arr = [1, 2, 3]
id.set(arr, 'arr')
const obj = { a: 1, b: 2 }
id.set(obj, 'obj')

tap.is(id.match(arr), 'arr')
tap.is(id.match(arr.slice()), null, 'In javascript, `[] !== []`')

tap.is(id.match(obj), 'obj')
tap.is(id.match({ a: 1, b: 2 }), null, 'In javascript, `{} !== {}`')

// Calling `set` a second time with the same pattern overrides the old value
id.set('Abc', 789)
tap.is(id.match('Abc'), 789, 'Used to be "Abc"')

// You can use any object for patterns and/or values
const dub = a => 2 * a
id.set('double', dub)
id.set(dub, 2)
tap.is(id.match('double'), dub)
tap.is(id.match(dub), 2)
tap.is(id.match(a => 2 * a), null, 'In javascript, `() => {} !== () => {}`')
// #snip
