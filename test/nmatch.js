/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict'
/* eslint no-multi-spaces: "off" */

const tap = require('tap')

const Paths = require('../matchers/paths')

// #snip "intro"
// NMatch lets you combine multiple pattern matchers.
//
// Say you have a set of URL patterns, like `"/foo"` and `"/foo/{id}"`,
// and a set of HTTP method patterns, like `"GET"`, `"POST"`, and `"ANY"`,
// and a set of user role patterns, like "`{ user: "alice" }` and
// `{ group: "admins" }`. You can bundle these into NMatch and then ask it
// "what's the handler for this user doing this method to this URL?"
//
// You do this in three steps.
//
// First you define what your rules will look like by choosing a set of
// matchers:
// #snip

// Uh let's build the matchers we want to use in our example:
//
// #snip "matcher intro"
// Any object that implements the following two methods is a matcher:
//
// `function pattern (pattern)` stores the given pattern in the matcher, and
// returns a container object on which we can set its value. `NMatch.set` calls
// this method.
//
// `function * match (instance)` yields a list of matches for the given
// instance, ordered from best match to worst. `NMatch.match` calls this
// method.
//
// There's no restriction on the type of argument taken by these functions. The
// one limitation is that they always get called with a single argument. The
// rest is up to you, and the semantics are pretty flexible.
//
// For example, here's the implementation of the three matchers used in the
// intro:
class UrlMatcher extends Paths {
  constructor () {
    super('/', /^\{\w+\}$/, /^\{\w+\+\}$/)
  }
}

class MethodMatcher extends Paths {
  constructor () {
    super(null, 'ANY')
  }
}

class UserRoleMatcher {
  constructor () {
    this._users = new Map()
  }

  pattern (user) {
    let u = this._users.get(user)
    if (u === undefined) {
      u = {}
      this._users.set(user, u)
    }

    return u
  }

  * match (term) {
    for (let [user, u] of this._users.entries()) {
      if (term.groups.includes(user.group)) {
        yield u
      }
    }
  }
}
// #snip

const getFoosHandler = 1
const getAFooHandler = 2
const editFooHandler = 3
const defaultHandler = 4

// Ok, we have our matchers defined. Back to our example!

// #snip "intro II"
const NMatch = require('../')

const router = new NMatch([
  () => new UrlMatcher(),
  () => new MethodMatcher(),
  () => new UserRoleMatcher()
])

// Then you set rules that map patterns to results. Each rule takes one pattern
// per matcher, plus the result that the rule should return when it matches:
router.set('/foo',       'GET', { group: 'public' }, getFoosHandler)
router.set('/foo/{id}',  'GET', { group: 'public' }, getAFooHandler)
router.set('/foo/{id}',  'PUT', { group: 'admins' }, editFooHandler)
router.set('{default+}', 'ANY', { group: 'public' }, defaultHandler)

// Now you can match terms against the ruleset, one term per matcher. NMatch
// will return the best result that matches all the terms: the first term
// against the first matcher, the second term against the second matcher, and
// so on.
const alice = { 'username': 'alice', 'groups': ['public', 'admins'] }
const anon = { 'groups': ['public'] }

tap.is(router.match('/foo/bar', 'GET', anon), getAFooHandler)
tap.is(router.match('/foo/bar', 'PUT', anon), defaultHandler)
tap.is(router.match('/foo/bar', 'PUT', alice), editFooHandler)

// API
// ---
//
// **The NMatch constructor** takes for argument an array of functions, each of
// which should return a new instance of a matcher. They will be called as
// needed when more patterns are added. Their order is the same as that of the
// `set` and `match` methods' arguments.
const Id = require('../matchers/id') // "matches" using `===`

const bookNotes = new NMatch([
  () => new Id(), // Author
  () => new Id()  // Title
])

// **The `set` method** takes as many arguments as you've defined matchers, plus
// one for the value being set. `set` will associate that value to the given
// patterns. What counts as a pattern depends on the kind of matcher you use.

bookNotes.set('Homer', 'Odyssey', 'Really old')
bookNotes.set('James Joyce', 'Ulysses', 'Just here for the puns')

// **The `match` method** takes as many arguments as you've defined matchers.
// It will pass the first argument to the first matcher, the second argument to
// the second matcher, and so on. It returns the best match, or `null` if no
// matches are found.

tap.is(bookNotes.match('Homer', 'Odyssey'), 'Really old')
tap.is(bookNotes.match('Homer', 'Ulysses'), null) // no match
// #snip

// As a more realistic example, here's a simple authorization system
// (the `Paths` matcher does wildcard matching on path elements)
const authz = new NMatch([
  () => new Paths(), // resource
  () => new Paths(), // action
  () => new Id() // actor
])

authz.set('/foobar',           'GET',  'public', 1)
authz.set('/foobar/*',         'GET',  'public', 2)
authz.set('/foobar',           'POST', 'editor', 3)
authz.set('/foobar/*',         'PUT',  'editor', 4)
authz.set('/private/**',       '*',    'admin',  5)
authz.set('/private/alice/**', '*',    'alice',  6)

tap.is(authz.match('/foobar',            'GET',   'public'), 1)
tap.is(authz.match('/foobar',            'POST',  'public'), null)
tap.is(authz.match('/foobar/baz',        'GET',   'public'), 2)
tap.is(authz.match('/foobar',            'POST',  'editor'), 3)
tap.is(authz.match('/foobar/baz',        'PUT',   'public'), null)
tap.is(authz.match('/foobar/baz',        'PUT',   'editor'), 4)
tap.is(authz.match('/private/alice',     'POST',  'admin'),  5)
tap.is(authz.match('/private/alice/foo', 'PATCH', 'alice'),  6)

// #snip "details"
// The NMatch constructor does some basic validation on its arguments
tap.throws(() => new NMatch(), 'At least one matcher is required')
tap.throws(() => new NMatch([]), 'At least one matcher is required')
tap.throws(() => new NMatch(['herp']), 'Matchers must be functions')
tap.throws(() => new NMatch([() => 'derp']),
  'Matchers must have a `pattern` and a `match` function')
tap.throws(() => new NMatch([() => ({ pattern: () => 1 })]),
  'Matchers must have a `pattern` and a `match` function')
tap.throws(() => new NMatch([() => ({ match: () => 1 })]),
  'Matchers must have a `pattern` and a `match` function')

// `set` and `match` will check that you pass in the right number of arguments,
// but they can't tell whether you've given them in the right order. Make sure
// to be consistent.
tap.throws(() => bookNotes.set('Homer', 'Iliad'),
  'Too few arguments')
tap.throws(() => bookNotes.set('Homer', 'Iliad', 'Trojan war', 'Prequel'),
  'Too many arguments')

tap.throws(() => bookNotes.get('Homer'),
  'Too few arguments')
tap.throws(() => bookNotes.get('Homer', 'Iliad', 'Odyssey'),
  'Too many arguments')

bookNotes.set('Iliad', 'Homer', 'Trojan war') /* Oops, arguments in wrong order,
  but NMatch has no way of knowing */
tap.is(bookNotes.match('Homer', 'Iliad'), null, 'No match: order matters')
// #snip
